#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "AI/Navigation/NavFilters/NavigationQueryFilter.h"
#include "AI/Navigation/NavigationData.h"
#include "TrafficLibrary.generated.h"

UCLASS(Within = World)
class TRAFFIC_API UTrafficLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
	UFUNCTION(BlueprintPure, Category = "AI|Navigation", meta = (WorldContext = "WorldContext", DisplayName = "GetRandomPoint"))
	static bool K2_GetRandomPoint(UObject* WorldContext, FVector& RandomLocation, ANavigationData* NavData = NULL, TSubclassOf<UNavigationQueryFilter> FilterClass = NULL);

	UFUNCTION(BlueprintPure)
	static bool IsInside(UBoxComponent* box, FVector point);
};
