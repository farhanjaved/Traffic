
#pragma once

#include "CoreMinimal.h"
#include "VisualLogger/VisualLogger.h"
#include "Runtime/AIModule/Classes/Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Services/BTService_RunEQS.h"
#include "BTService_RunEQS_Clear.generated.h"


UCLASS()
class TRAFFIC_API UBTService_RunEQS_Clear : public UBTService_RunEQS
{
	GENERATED_BODY()

public:
	UBTService_RunEQS_Clear(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

protected:
	void OnQueryFinished(TSharedPtr<FEnvQueryResult> Result);
	
};
