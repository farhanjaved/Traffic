#include "BTService_RunEQS_Clear.h"

UBTService_RunEQS_Clear::UBTService_RunEQS_Clear(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	QueryFinishedDelegate = FQueryFinishedSignature::CreateUObject(this, &UBTService_RunEQS_Clear::OnQueryFinished);
}

void UBTService_RunEQS_Clear::OnQueryFinished(TSharedPtr<FEnvQueryResult> Result)
{
	if (Result->IsAborted())
	{
		return;
	}

	AActor* MyOwner = Cast<AActor>(Result->Owner.Get());
	if (APawn* PawnOwner = Cast<APawn>(MyOwner))
	{
		MyOwner = PawnOwner->GetController();
	}

	UBehaviorTreeComponent* BTComp = MyOwner ? MyOwner->FindComponentByClass<UBehaviorTreeComponent>() : NULL;
	if (!BTComp)
	{
		UE_LOG(LogBehaviorTree, Warning, TEXT("Unable to find behavior tree to notify about finished query from %s!"), *GetNameSafe(MyOwner));
		return;
	}

	FBTEQSServiceMemory* MyMemory = reinterpret_cast<FBTEQSServiceMemory*>(BTComp->GetNodeMemory(this, BTComp->FindInstanceContainingNode(this)));
	check(MyMemory);
	ensure(MyMemory->RequestID != INDEX_NONE);

	bool bSuccess = (Result->Items.Num() >= 1);
	UBlackboardComponent* MyBlackboard = BTComp->GetBlackboardComponent();
	if (bSuccess)
	{
		UEnvQueryItemType* ItemTypeCDO = Result->ItemType->GetDefaultObject<UEnvQueryItemType>();

		bSuccess = ItemTypeCDO->StoreInBlackboard(BlackboardKey, MyBlackboard, Result->RawData.GetData() + Result->Items[0].DataOffset);
		if (!bSuccess)
		{
			UE_VLOG(MyOwner, LogBehaviorTree, Warning, TEXT("Failed to store query result! item:%s key:%s"),
				*UEnvQueryTypes::GetShortTypeName(Result->ItemType).ToString(),
				*UBehaviorTreeTypes::GetShortTypeName(BlackboardKey.SelectedKeyType));
		}
	}
	else
	{
		MyBlackboard->ClearValue(BlackboardKey.SelectedKeyName);
	}
	MyMemory->RequestID = INDEX_NONE;
}