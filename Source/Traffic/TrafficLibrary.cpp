#include "TrafficLibrary.h"
#include "Engine.h"
#include "Components/BoxComponent.h"
#include "AI/Navigation/NavigationSystem.h"

bool UTrafficLibrary::K2_GetRandomPoint(UObject* WorldContext, FVector& RandomLocation, ANavigationData* NavData, TSubclassOf<UNavigationQueryFilter> FilterClass)
{
	FNavLocation RandomPoint;
	bool bResult = false;

	UWorld* World = GEngine->GetWorldFromContextObject(WorldContext);
	UNavigationSystem* NavSys = UNavigationSystem::GetCurrent(World);
	if (NavSys)
	{
		ANavigationData* UseNavData = NavData ? NavData : NavSys->GetMainNavData(FNavigationSystem::DontCreate);
		if (UseNavData)
		{
			bResult = NavSys->GetRandomPoint(RandomPoint, UseNavData, UNavigationQueryFilter::GetQueryFilter(*UseNavData, WorldContext, FilterClass));
			RandomLocation = RandomPoint.Location;
		}
	}
	return bResult;
}

bool UTrafficLibrary::IsInside(UBoxComponent* box, FVector point)
{
	return box->CalcBounds(box->GetComponentTransform()).ComputeSquaredDistanceFromBoxToPoint(point) <= 0;
}